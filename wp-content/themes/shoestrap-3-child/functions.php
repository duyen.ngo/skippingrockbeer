<?php

// Include custom template files
add_action( 'shoestrap_include_files', 'include_template_files' );
function include_template_files() {
	include_once locate_template( 'templates/top-bar.php' );
}

// Add stylesheet files
add_action( 'wp_enqueue_scripts', 'enqueue_stylesheets', 200 );
function enqueue_stylesheets() {
	wp_enqueue_style( 'bxslider-style', get_stylesheet_directory_uri() . '/assets/css/jquery.bxslider.css' );
	wp_enqueue_style( 'default-style', get_stylesheet_directory_uri() . '/assets/css/default-style.css' );
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css' );
	wp_enqueue_style( 'responsive', get_stylesheet_directory_uri() . '/responsive.css' );
}

// Add script files
add_action( 'wp_enqueue_scripts', 'enqueue_scripts' );
function enqueue_scripts() {
	wp_enqueue_script( 'bxslider-script', get_stylesheet_directory_uri() . '/assets/js/jquery.bxslider.min.js', false, null, true );
	wp_enqueue_script( 'default-script', get_stylesheet_directory_uri() . '/assets/js/default-script.js', false, null, true );
	wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/script.js', false, null, true );
	wp_enqueue_script( 'abc', get_stylesheet_directory_uri() . '/assets/js/isotope.pkgd.min.js', false, null, true);
}

// Remove inline stlyle in header
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// Add confirmation when remove acf item in repeater field or flexible content field 
add_action( 'acf/input/admin_head', 'remove_acf_item_confirmation' );
function remove_acf_item_confirmation() {
	?>
    <script type="text/javascript">
        (function ($) {
            acf.add_action('ready', function () {
                $('body').on('click', '.acf-repeater-remove-row, .acf-fc-layout-controlls .-minus', function () {
                    return confirm("Delete row?");
                });
            });
        })(jQuery);
    </script>
	<?php
}

// Create section tag with id, class and inline-style
function open_section( $id, $classes = array(), $styles = array() ) {

	$class_arr = array();
	if ( ! empty( $classes ) ) {
		foreach ( $classes as $class ) {
			$class_arr[] = $class;
		}
		$extra_classes = implode( ' ', $class_arr );
	} else {
		$extra_classes = null;
	}

	$inline_style = "";
	if ( ! empty( $styles ) ) {
		foreach ( $styles as $key => $value ) {
			$inline_style .= "$key:$value;";
		}
	}

	echo "<section id='$id' class='page-section $extra_classes' style='$inline_style'>";
}

function close_section() {
	echo "</section>";
}


function get_section_title( $h1, $title, $sub_title ) {
	if ( $h1 == "section_title" ) {
		if ( $title ) {
			echo "<h1 class='section-title'>$title</h1>";
		}
		if ( $sub_title ) {
			echo "<h2 class='section-sub-title'>$sub_title</h2>";
		}
	} elseif ( $h1 == "section_sub_title" ) {
		if ( $title ) {
			echo "<h2 class='section-title'>$title</h2>";
		}
		if ( $sub_title ) {
			echo "<h1 class='section-sub-title'>$sub_title</h1>";
		}
	} else {
		if ( $title ) {
			echo "<h2 class='section-title'>$title</h2>";
		}
		if ( $sub_title ) {
			echo "<h4 class='section-sub-title'>$sub_title</h4>";
		}
	}
}

// Add Setting page for contact info
if ( function_exists( 'acf_add_options_page' ) ) {

	acf_add_options_page( array(
		'page_title' => 'Contact Settings',
		'menu_title' => 'Contact Settings',
		'menu_slug'  => 'contact-settings',
		'capability' => 'edit_posts',
		'redirect'   => true
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'General Info',
		'menu_title'  => 'General Info',
		'parent_slug' => 'contact-settings'
	) );

	acf_add_options_sub_page( array(
		'page_title'  => 'Social Media Channels',
		'menu_title'  => 'Social Media Channels',
		'parent_slug' => 'contact-settings'
	) );
}

// Add thumbnail for section image on top and bottom
add_theme_support( 'full-width-image', 'page' );
add_image_size( 'full-width-image', 1920, 500, true );

add_theme_support( 'grid-card-thumbnail', 'page' );
add_image_size( 'grid-card-thumbnail', 1024, 720, true );


// Social channel shortcode
function social_channel() {
	$channel_facebook    = get_field( 'channel_facebook', 'option' );
	$channel_instagram   = get_field( 'channel_instagram', 'option' );
	$channel_twitter     = get_field( 'channel_twitter', 'option' );
	$channel_youtube     = get_field( 'channel_youtube', 'option' );
	$channel_googleplus  = get_field( 'channel_googleplus', 'option' );
	$channel_linkedin    = get_field( 'channel_linkedin', 'option' );
	$channel_pinterest   = get_field( 'channel_pinterest', 'option' );
	$channel_tripadvisor = get_field( 'channel_tripadvisor', 'option' );
	?>

    <ul class="channel-list">
		<?php
		if ( $channel_facebook ) {
			echo "<li class='channel-item'><a target='_blank' class='channel channel-facebook' href='" . $channel_facebook . "'> <img alt='facebook' class='svg channel-logo' src='" . get_stylesheet_directory_uri() . "/assets/img/facebook.svg' /> </a></li>";
		}
		if ( $channel_instagram ) {
			echo "<li class='channel-item'><a target='_blank' class='channel channel-instagram' href='" . $channel_instagram . "'><img alt='instagram' class='svg channel-logo' src='" . get_stylesheet_directory_uri() . "/assets/img/instagram.svg' /></a></li>";
		}
		if ( $channel_twitter ) {
			echo "<li class='channel-item'><a target='_blank' class='channel channel-twitter' href='" . $channel_twitter . "'><img alt='twitter' class='svg channel-logo' src='" . get_stylesheet_directory_uri() . "/assets/img/twitter.svg' /></a></li>";
		}
		if ( $channel_youtube ) {
			echo "<li class='channel-item'><a target='_blank' class='channel channel-youtube' href='" . $channel_youtube . "'> <img alt='youtube' class='svg channel-logo' src='" . get_stylesheet_directory_uri() . "/assets/img/youtube.svg' /> </a></li>";
		}
		if ( $channel_googleplus ) {
			echo "<li class='channel-item'><a target='_blank' class='channel channel-google' href='" . $channel_googleplus . "'> <img alt='googleplus' class='svg channel-logo' src='" . get_stylesheet_directory_uri() . "/assets/img/googleplus.svg' /> </a></li>";
		}
		if ( $channel_linkedin ) {
			echo "<li class='channel-item'><a target='_blank' class='channel channel-linkedin' href='" . $channel_linkedin . "'> <img alt='linkedin' class='svg channel-logo' src='" . get_stylesheet_directory_uri() . "/assets/img/linkedin.svg' /> </a></li>";
		}
		if ( $channel_pinterest ) {
			echo "<li class='channel-item'><a target='_blank' class='channel channel-pinterest' href='" . $channel_pinterest . "'> <img alt='pinterest' class='svg channel-logo' src='" . get_stylesheet_directory_uri() . "/assets/img/pinterest.svg' /> </a></li>";
		}
		if ( $channel_tripadvisor ) {
			echo "<li class='channel-item'><a target='_blank' class='channel channel-tripadvisor' href='" . $channel_tripadvisor . "'> <img alt='tripadvisor' class='svg channel-logo' src='" . get_stylesheet_directory_uri() . "/assets/img/tripadvisor.svg' /> </a></li>";
		}

		?>
    </ul>
	<?php
}

add_shortcode( 'social-channel', 'social_channel' );


function sidebar_nav_template() {
	// Check if the sections field has rows of data
	if ( have_rows( 'sections' ) ):
		// Loop through the sections
		?>
        <ul class="sidebar-nav">

			<?php
			while ( have_rows( 'sections' ) ) : the_row();
				// Get section template base on section type
				$section_id    = get_sub_field( 'section_id' );
				$section_title = get_sub_field( 'section_title' );

				if ( $section_id && $section_title ):
					?>
                    <li class="sidebar-nav-item">
                        <a href="#<?php echo $section_id; ?>"><?php echo $section_title; ?></a>
                    </li>
					<?php
				endif;

			endwhile;
			?>

        </ul>

		<?php
	endif;
}

add_shortcode( 'sidebar-nav', 'sidebar_nav_template' );


/*
* Function to check image's filesize
* Allowed size: < 1MB
*
*/
add_filter('wp_handle_upload_prefilter', 'image_size_prevent');
function image_size_prevent($file) {
   $size = $file['size'];
   $size = $size / 1024; // Calculate down to KB
   $type = $file['type'];
   $is_image = strpos($type, 'image');
   $limit = 1000; // Your Filesize in KB

   if ( ( $size > $limit ) && ($is_image !== false) ) {
       $file['error'] = 'Image files must be smaller than '.$limit.'KB';
   }

   return $file;

}

// Hotfix
add_filter( 'wp_update_attachment_metadata', 'rips_unlink_tempfix' );

function rips_unlink_tempfix( $data ) {
    if( isset($data['thumb']) ) {
        $data['thumb'] = basename($data['thumb']);
    }

    return $data;
}

function wpb_rand_posts() { 
    $args = array(
    'post_type' => 'post',
    'posts_per_page' => 3, 
    );
    $the_query = new WP_Query( $args );
    if ( $the_query->have_posts() ) {
    $string = '<div class="our-beers-listing">';
        while ( $the_query->have_posts() ) {
		$the_query->the_post();
		$string .= '<div class="our-beer">';
			$string .= '<div class="beer-image">';
				$string .= '<img src="'.get_the_post_thumbnail();'">';
			$string .= '</div>';
			$string .= '<div class="beer-info">';
				$string .= '<div class="beer-name">';
					$string .= '<a href="'. get_permalink() .'">'. get_the_title() .'</a>';
				$string .= '</div>';
			$string .= '</div>';
		$string .= '</div>';
        }
    $string .= '</div>';
    /* Restore original Post Data */
    wp_reset_postdata();
    } else {
    $string .= 'no posts found';
    }
    return $string; 
} 
add_shortcode('our-beers','wpb_rand_posts');



function show_listing_beers(){
    ob_start();
    ?>
    <div id="beers-listing" class="row our-beers-listing">
    <?php
    $categories = get_categories( array(
        'orderby' => 'name',
        'parent'  => 0
    ) );
     
    foreach ( $categories as $category ) {

        $args = array(
            'post_type' => 'beer',
            'category_name' => $category->slug,
            'orderby'   => 'date',
            'order'     => 'ASC',
        );
    
        $the_query = new WP_Query( $args ); ?>
        <?php if ( $the_query->have_posts() ) : ?>
            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                <div class="element-item our-beer col-md-4 <?php echo $category->slug; ?>">
                    <div class="beer-image">
                        <a><img src="<?php echo get_the_post_thumbnail_url(); ?>" alt=""></a>
                    </div>
                    <div class="beer-info">
                        <div class="beer-na"> 
                            <a href="#"> <?php the_title(); ?> </a>
                        </div>
                        <div class="beer-volume"> <?php the_field('beer_volume'); ?> </div>
                        <div class="beer-excerpt">
                            <p><span><?php $content =  get_the_excerpt();  echo substr($content, 0, 100); ?>...</span></p>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
            <?php wp_reset_postdata(); ?>
    
        <?php else : ?>
            <p><?php esc_html_e( 'No post founded' ); ?></p>
        <?php endif; 
    } ?>
    </div>
    <?php

    return ob_get_clean();
}add_shortcode( 'listing-beers', 'show_listing_beers' );

