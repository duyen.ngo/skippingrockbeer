<?php

// Get fields that associated with section

$type = get_row_layout();

$layout = get_sub_field($type.'_layout');

$height = get_sub_field('section_default_height');
$height_custom = get_sub_field('section_default_height_custom');
$text_color = get_sub_field('section_text_color');
$background_color = get_sub_field('section_background_color');
$background_image = get_sub_field('section_background_image');
$background_image_url = $background_image['url'];
$text_align = get_sub_field('section_text_align');

$h1 = get_sub_field('section_h1');

$title = get_sub_field('section_title');
$sub_title = get_sub_field('section_sub_title');
$description = get_sub_field('section_description');
$body = get_sub_field('section_body');

// Initial id, class and inline-style to push into section
$id = get_sub_field('section_id');
$classes = array();
$styles = array();

// Add section type to class
if ( $type ) {
	$classes[] = $type;
}

if ( $text_align ) {
	$classes[] = 'text-'.$text_align;
}

// Add section layout to class
if ( $layout ) {
	$classes[] = $type.'_layout_'.$layout;
}

// Add height to class and inline-style
$height_type = "";
$height_number = "";
if ( $height ) {
	$height_type = $height;
	if ( $height == "custom-height" && $height_custom ) {
		$height_number = 'height:'.$height_custom.'px;';
	}
}

// Add background color to inline-style
if ( $background_color ) {
	$styles['background-color'] = $background_color;
}

if ( $text_color ) {
	$styles['color'] = $text_color;
}

// Add background image to inline-style
if ( $background_image ) {
	$styles['background-image'] = "url($background_image_url)";
}

// Create section with id, class and inline-style
open_section($id, $classes, $styles);
		// Get template of seleted layout by file name
		include(locate_template( "templates/sections/$type/$layout.php" ) );
close_section();

