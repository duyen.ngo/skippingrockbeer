<div class="section-inner <?php echo $height_type; ?>" style="<?php echo $height_number; ?>">

	<div class="section-content-wrapper">
		<div class="section-content">
			<?php get_section_title($h1, $title, $sub_title); ?>

			<?php if ($description): ?>
				<div class="section-description"><?php echo apply_filters('the_content', $description); ?></div>
			<?php endif; ?>

			<?php if ($body): ?>
				<div class="section-body"><?php echo apply_filters('the_content', $body); ?></div>
			<?php endif; ?>
		</div>
	</div>

	<?php if ( count($slider) != 0 ): ?>
		<div class="section-slider background">
			<ul class="bxslider">
				<?php foreach ($slider as $key => $value):
					$image = $value['section_background_slider_image'];
				?>
					<li class="slide" style="background-image: url(<?php echo $image['url']; ?>);"></li>    
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>
	

</div>