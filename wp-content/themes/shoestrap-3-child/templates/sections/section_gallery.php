<?php

// Get fields that associated with section

$type = get_row_layout();

$layout = get_sub_field($type.'_layout');


$h1 = get_sub_field('section_h1');

$title = get_sub_field('section_title');
$body = get_sub_field('section_body');
$gallery = get_sub_field('section_gallery_images');
// Initial id, class and inline-style to push into section
$id = get_sub_field('section_id');
$classes = array();
$styles = array();

// Add section type to class
if ( $type ) {
    $classes[] = $type;
}

// Add section layout to class
if ( $layout ) {
    $classes[] = $type.'_layout_'.$layout;
}

if ( $background_color ) {
    $styles['background-color'] = $background_color;
}

if ( $text_color ) {
    $styles['color'] = $text_color;
}

// Create section with id, class and inline-style
open_section($id, $classes, $styles);
        // Get template of seleted layout by file name
        include(locate_template( "templates/sections/$type/$layout.php" ) );
close_section();