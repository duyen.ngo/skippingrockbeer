<?php

// Get fields that associated with section

$type = get_row_layout();

$layout = get_sub_field($type.'_layout');

$height = get_sub_field('section_background_slider_height');
$height_custom = get_sub_field('section_background_slider_height_custom');

$slider = get_sub_field('section_background_slider_slider');
$text_align = get_sub_field('section_text_align');

$h1 = get_sub_field('section_h1');

$title = get_sub_field('section_title');
$sub_title = get_sub_field('section_sub_title');
$description = get_sub_field('section_description');
$body = get_sub_field('section_body');

// Initial id, class and inline-style to push into section
$id = get_sub_field('section_id');
$classes = array();
$styles = array();

// Add section type to class
if ( $type ) {
	$classes[] = $type;
}

// Add section layout to class
if ( $layout ) {
	$classes[] = $type.'_layout_'.$layout;
}

if ( $text_align ) {
	$classes[] = 'text-'.$text_align;
}

// Add height to class and inline-style
$height_type = "";
$height_number = "";
if ( $height ) {
	$height_type = $height;
	if ( $height == "custom-height" && $height_custom ) {
		$height_number = 'height:'.$height_custom.'px;';
	}
}

// Create section with id, class and inline-style
open_section($id, $classes, $styles);
		// Get template of seleted layout by file name
		include(locate_template( "templates/sections/$type/$layout.php" ) );
close_section();