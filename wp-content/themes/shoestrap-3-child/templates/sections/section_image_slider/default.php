<div class="section-inner">

	<div class="section-header">
	
		<?php get_section_title($h1, $title, $sub_title); ?>

		<?php if ($description): ?>
			<div class="section-description"><?php echo apply_filters('the_content', $description); ?></div>
		<?php endif; ?>

	</div>

	<?php if ( count($slider) != 0 ): ?>
		<div class="section-slider image">
			<ul class="bxslider">
				<?php foreach ($slider as $key => $value):
					$image = $value['section_image_slider_image'];
				?>
					<?php if ($image): ?>
						<li class="slide">
							<img src="<?php echo $image['url']; ?>" />
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>

	<div class="section-content">
		<?php if ($body): ?>
			<div class="section-body"><?php echo apply_filters('the_content', $body); ?></div>
		<?php endif; ?>
	</div>

</div>