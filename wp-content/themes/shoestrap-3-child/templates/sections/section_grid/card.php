<div class="section-inner">

    <div class="section-header">
		<?php get_section_title( $h1, $title, $sub_title ); ?>

		<?php if ( $description ): ?>
            <div class="section-description"><?php echo apply_filters( 'the_content', $description ); ?></div>
		<?php endif; ?>
    </div>

    <div class="section-grid">
		<?php if ( isset( $grid_items ) && is_array($grid_items) && count( $grid_items ) != 0 ) { ?>
            <ul class="grid-list">
				<?php foreach ( $grid_items as $key => $value ) {
					$image       = $value['section_grid_items_image'];
					$title       = $value['section_grid_items_title'];
					$description = $value['section_grid_items_description'];
					?>
                    <li class="grid-item card col-md-<?php echo $column_number; ?>">

                        <div class="grid-item-inner">
                            <img class="grid-item-img" src="<?php echo $image['sizes']['grid-card-thumbnail']; ?>"/>
                            <div class="grid-item-text">
								<?php if ( $title ): ?>
                                    <h4 class="grid-item-title"><?php echo $title; ?></h4>
								<?php endif; ?>
								<?php if ( $description ): ?>
                                    <div class="grid-item-description"><?php echo apply_filters( 'the_content', $description ); ?></div>
								<?php endif; ?>
                            </div>
                        </div>

                    </li>
				<?php } ?>
            </ul>
		<?php } ?>
    </div>

</div>