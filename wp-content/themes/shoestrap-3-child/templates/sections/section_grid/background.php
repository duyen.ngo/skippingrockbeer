<div class="section-inner <?php echo $height_type; ?>" style="<?php echo $height_number; ?>">

	<div class="section-grid">
	<?php if ( count($grid_items) != 0 ): ?>
		<ul class="grid-list">
			<?php foreach ($grid_items as $key => $value):
        $image = $value['section_grid_items_image'];
        $title = $value['section_grid_items_title'];
        $description = $value['section_grid_items_description'];
			?>
				<li class="grid-item background col-md-<?php echo $column_number; ?>" style="background-image: url(<?php echo $image['url']; ?>);">

					<div class="grid-item-inner">
						<div class="grid-item-text">
							<?php if ($title): ?>
								<h4 class="grid-item-title"><?php echo $title; ?></h4>
							<?php endif; ?>
							<?php if ($description): ?>
								<div class="grid-item-description"><?php echo apply_filters('the_content', $description); ?></div>
							<?php endif; ?>
						</div>
					</div>

				</li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
	</div>

</div>