<?php

// Get fields that associated with section

$type = get_row_layout();

$layout = get_sub_field($type.'_layout');

$height = get_sub_field('section_content_slider_height');
$height_custom = get_sub_field('section_content_slider_height_custom');
$text_align = get_sub_field('section_text_align');

$slider = get_sub_field('section_content_slider_slider');

// Initial id, class and inline-style to push into section
$id = get_sub_field('section_id');
$classes = array();
$styles = array();

// Add section type to class
if ( $type ) {
	$classes[] = $type;
}

if ( $text_align ) {
	$classes[] = 'text-'.$text_align;
}

// Add section layout to class
if ( $layout ) {
	$classes[] = $type.'_layout_'.$layout;
}

// Add height to class and inline-style
$height_type = "";
$height_number = "";
if ( $height ) {
	$height_type = $height;
	if ( $height == "custom-height" && $height_custom ) {
		$height_number = 'height:'.$height_custom.'px;';
	}
}

// Create section with id, class and inline-style
open_section($id, $classes, $styles);
		// Get template of seleted layout by file name
		include(locate_template( "templates/sections/$type/$layout.php" ) );
close_section();