<div class="section-inner <?php echo $height_type; ?>" style="<?php echo $height_number; ?>">

<?php if ( count($slider) != 0 ): ?>
	<div class="section-slider background">
		<ul class="bxslider">
			<?php foreach ($slider as $key => $value):
        $image = $value['section_content_slider_image'];
        $title = $value['section_content_slider_title'];
        $description = $value['section_content_slider_description'];
			?>
				<li class="slide" style="background-image: url(<?php echo $image['url']; ?>);">

					<div class="section-content-wrapper">
						<div class="section-content">
							<?php if ($title): ?>
								<h2 class="slide-title"><?php echo $title; ?></h2>
							<?php endif; ?>
							<?php if ($description): ?>
								<div class="slide-description"><?php echo apply_filters('the_content', $description); ?></div>
							<?php endif; ?>
						</div>
					</div>

				</li>
			<?php endforeach; ?>
		</ul>
	</div>
<?php endif; ?>

</div>