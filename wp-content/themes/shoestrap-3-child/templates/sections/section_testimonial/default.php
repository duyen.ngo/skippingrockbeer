<div class="section-inner">

	<div class="section-header">
	
		<?php get_section_title($h1, $title, $sub_title); ?>

		<?php if ($description): ?>
			<div class="section-description"><?php echo apply_filters('the_content', $description); ?></div>
		<?php endif; ?>

	</div>

	<?php if ( count($testimonials) != 0 ): ?>
		<div class="section-slider text">
			<ul class="bxslider">
				<?php foreach ($testimonials as $key => $value):
					$avatar = $value['section_testimonial_avatar'];
					$name = $value['section_testimonial_name'];
					$info = $value['section_testimonial_info'];
					$description = $value['section_testimonial_description'];
				?>
					<?php if ($value): ?>
						<li class="slide testimonial-item">

							<?php if ($description): ?>
								<div class="testimonial-description"> <?php echo apply_filters('the_content', $description); ?> </div>
							<?php endif; ?>

							<?php if ($avatar): ?>
								<img src="<?php echo $avatar['url']; ?>" />
							<?php endif; ?>
							
							<?php if ($name): ?>
								<div class="testimonial-name"> <?php echo $name; ?> </div>
							<?php endif; ?>

							<?php if ($info): ?>
								<div class="testimonial-info"> <?php echo $info; ?> </div>
							<?php endif; ?>

						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	<?php endif; ?>


</div>