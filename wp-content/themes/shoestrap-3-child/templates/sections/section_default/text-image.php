<?php
if (! function_exists('get_image_template')) {
	function get_image_template($position) {
		
		$image = get_sub_field('section_default_image');
		$image_position = get_sub_field('section_layout_image_position');

		if ($image_position == 'left' || $image_position == 'right') {
			$image_url = $image['url'];
			$class = 'side-image col-md-6';
		} elseif ($image_position == 'top' || $image_position == 'bottom') {
			$image_url = $image['sizes']['full-width-image'];
			$class = 'full-width-image';
		}

		if ($position == $image_position) {
			echo "<div class='section-image $class'><img src='$image_url' /></div>";
		}
	}
}
?>

<?php
	$image_position = get_sub_field('section_layout_image_position');
?>

<?php 
	get_image_template('top');
?>

<div class="section-inner <?php echo $height_type; ?>" style="<?php echo $height_number; ?>">

		<?php
			get_image_template('left');
		?>
		<div class="section-content <?php echo ($image_position == 'left' || $image_position == 'right') ? 'side-content col-md-6' : '' ; ?>">
			<?php get_section_title($h1, $title, $sub_title); ?>

			<?php if ($description): ?>
				<div class="section-description"><?php echo apply_filters('the_content', $description); ?></div>
			<?php endif; ?>

			<?php if ($body): ?>
				<div class="section-body"><?php echo apply_filters('the_content', $body); ?></div>
			<?php endif; ?>
		</div>
		<?php 
			get_image_template('right');
		?>

</div>

<?php 
	get_image_template('bottom');
?>
