<div class="section-inner <?php echo $height_type; ?>" style="<?php echo $height_number; ?>">
	<div class="section-content-wrapper">
		<div class="section-content">
			<div class="section-header-info">
				<?php get_section_title($h1, $title, $sub_title); ?>
				<?php if ($description): ?>
					<div class="section-description"><?php echo apply_filters('the_content', $description); ?></div>
				<?php endif; ?>
			</div>
			<?php if ($body): ?>
				<div class="section-body">
					<?php echo apply_filters('the_content', $body); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>