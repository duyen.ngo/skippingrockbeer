<?php

	function get_body_image_template($position) {
		
		$image = get_sub_field('section_default_image');
		$image_position = get_sub_field('section_layout_image_cover_position');

		$image_url = $image['url'];
		$class = 'side-image col-md-6';

		if ($position == $image_position) {
			echo "<div class='section-image $class'><img src='$image_url' /></div>";
		}
	}
?>

<div class="section-inner <?php echo $height_type; ?>" style="<?php echo $height_number; ?>">

	<div class="section-header">
		<?php get_section_title($h1, $title, $sub_title); ?>

		<?php if ($sub_title): ?>
			<h4 class="section-sub-title"><?php echo $sub_title; ?></h4>
		<?php endif; ?>
		
		<?php if ($description): ?>
			<div class="section-description"><?php echo apply_filters('the_content', $description); ?></div>
		<?php endif; ?>
	</div>

		<?php
			get_body_image_template('left');
		?>

		<div class="section-content side-content col-md-6">
			<?php if ($body): ?>
				<div class="section-body"><?php echo apply_filters('the_content', $body); ?></div>
			<?php endif; ?>
		</div>

		<?php 
			get_body_image_template('right');
		?>

</div>

