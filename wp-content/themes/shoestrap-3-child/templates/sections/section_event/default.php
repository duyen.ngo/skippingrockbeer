<div class="section-inner">
   <div class="featured-events-container">
       <div class="featured-events">
            <?php if ($event_image): ?>
               <img src="<?php echo $event_image['url']; ?>" />
            <?php endif; ?>
            <div class="events-right has-event">
                   <?php get_section_title($h1, $title, $sub_title); ?>
                   <div class="su-table tbl_schedule su-table-responsive">
                           <table>
                               <tbody>
                                   <?php if ( count($event_date_time) != 0):?>
                                       <?php foreach ($event_date_time as $key => $value):
                                           $date = $value['section_event_date'];
                                           $time = $value['section_event_time'];
                                       ?>
                                           <tr class="su-even">
                                               <td style="width: 150px; border-top: none;"><strong><?php echo $date; ?></strong></td>
                                               <td style="border-top: none;"><?php echo $time; ?></td>
                                           </tr>
                                       <?php endforeach; ?>
                                   <?php endif; ?>
                               </tbody>
                           </table>
                   </div>
            </div>
       </div>
   </div>
       <div class="section-content">
           <?php if ($body): ?>
               <div class="section-body"><?php echo apply_filters('the_content', $body); ?></div>
           <?php endif; ?>
       </div>
</div>