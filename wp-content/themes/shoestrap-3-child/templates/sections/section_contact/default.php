<div class="contact-map">
	<?php include_once locate_template("templates/sections/section_contact/map-template.php"); ?>
</div>

<div class="section-inner">

	
	<div class="section-content">
		<div class="contact-col contact-left col-md-6">
	     <div class="section-header">
        <?php get_section_title($h1, $title, $sub_title); ?>

        <?php if ($description): ?>
          <div class="section-description"><?php echo apply_filters('the_content', $description); ?></div>
        <?php endif; ?>
      </div>
		
		</div>

		<div class="contact-col contact-right col-md-6">
			<?php echo do_shortcode($body); ?>
		</div>
	</div>