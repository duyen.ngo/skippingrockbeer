<?php

	$coordinates = array();
	if ($location) {
		foreach ($location as $key => $value) {
			$coordinates[] = $value['latitude'].','.$value['longitude'];
		}
	}

?>	
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDwQ1do5uhLZtqazRBUjsyBCEn_6D6c4Sk"></script>
<script type="text/javascript">

	jQuery(document).ready(function($) {

		<?php echo 'var location_obj = ' . json_encode($location) . ';'; ?>
		if ( $(".tab-box .tab-link").length > 0 ) {
	    $(".tab-box .tab-link").each(function(index) {
				var lat = location_obj[0].latitude;
				var lng = location_obj[0].longitude;
				var location = new google.maps.LatLng(lat, lng);
				
				var isDraggable = jQuery(window).width() > 840 ? true : false;
				var options = {
					zoom: 18,
					center: location,
					scrollwheel: false,
					mapTypeControl: false,
					draggable: isDraggable, 
				};
				var map = new google.maps.Map(document.getElementById('map-canvas'),options);
				//var icon = new google.maps.MarkerImage("/wp-content/themes/shoestrap-3-child/assets/img/icon-map.png");
				var marker = new google.maps.Marker({
	 				map: map,
	 				//icon: icon,
	 				position: location
	 			});

	      $(this).click(function() {
	      	$(".tab-link").removeClass("active-map");
	        $(this).addClass("active-map");

	        var lat = location_obj[index].latitude;
					var lng = location_obj[index].longitude;
	        var location = new google.maps.LatLng(lat, lng);

	        var isDraggable = jQuery(window).width() > 840 ? true : false;
					var options = {
						zoom: 18,
						center: location,
						scrollwheel: false,
						mapTypeControl: false,
						draggable: isDraggable,
					};
	        var map = new google.maps.Map(document.getElementById('map-canvas'),options);
					//var icon = new google.maps.MarkerImage("/wp-content/themes/shoestrap-3-child/assets/img/icon-map.png");
					var marker = new google.maps.Marker({
		 				map: map,
		 				//icon: icon,
		 				position: location
		 			});
	    	});
			});
	  }
  });
</script>
	
	<div class="map-wrapper">
    <div class="tab-box">
    <?php
    if ($location) :
	    $i=0;
	    foreach ($location as $key => $value) :
	    	if ($i == 0) $active = "active-map";
	    	else $active = ""
	    ?>
	      <a href="javascript:;" class="tab-link <?php echo $active; ?>" id="cont-<?php echo $i; ?>"><?php echo $value['location_name'] ?></a>
  	<?php
  		$i++;
  		endforeach;
  	endif;
  	?>
    </div>
    <div class="tab-content"">
      <div id="map-canvas" class="map-canvas" style="width:100%; height:500px;"></div>
    </div>
  </div>