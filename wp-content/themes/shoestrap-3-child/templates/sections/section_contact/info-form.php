<div class="section-inner">

	<div class="section-content">
		<div class="contact-col contact-left col-md-6">

      <div class="section-header">
        <?php get_section_title($h1, $title, $sub_title); ?>

        <?php if ($description): ?>
          <div class="section-description"><?php echo apply_filters('the_content', $description); ?></div>
        <?php endif; ?>
      </div>
      
			<?php 
        if ($location) :
          foreach ($location as $key => $value) :
            $location_title = $value['location_title'];
            $address = $value['address'];
            $map_link = $value['map_link'];
            $telephone = $value['telephone'];
            $fax = $value['fax'];
            $email = $value['email'];
          ?>
            <div class="contact-info">
              <?php if ($location_title) : ?>
                <h6 class="contact-location-title"><?php echo $location_title; ?></h6>
              <?php endif; ?>
              <?php if ($address) : ?>
                <p class="contact-address"><?php echo $address; ?></p>
              <?php endif; ?>
              <?php if ($map_link) : ?>
                <p class="contact-map-link"><a target="_blank" href="<?php echo $map_link; ?>">View on map</a></p>
                <?php endif; ?>
              <?php if ($telephone) : ?>
                <p class="contact-telephone"><span>Phone: </span><a href="tel:<?php echo $telephone; ?>"><?php echo $telephone; ?></a></p>
              <?php endif; ?>
              <?php if ($fax) : ?>
                <p class="contact-fax"><span>Fax: </span><?php echo $fax; ?></p>
              <?php endif; ?>
              <?php if ($email) : ?>
                <p class="contact-email"><span>Email: </span><a href="mailto:<?php echo $email; ?>"><?php echo $email; ?></a></p>
              <?php endif; ?>
              <?php if ($note) : ?>
                <div class="contact-note"><?php echo $note; ?></div>
              <?php endif; ?>
            </div>
        <?php
          endforeach;
        endif;
        // Add shortcode get channel socials
        echo do_shortcode("[social-channel]"); 
      ?>
		</div>

		<div class="contact-col contact-right col-md-6">
			<?php echo do_shortcode($body); ?>
		</div>
	</div>