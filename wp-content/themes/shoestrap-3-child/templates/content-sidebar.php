<?php

$sidebar_widgets = get_field('sidebar_widgets');

if (count($sidebar_widgets) > 0 && $sidebar_widgets) {
	foreach ($sidebar_widgets as $widget) :
	?>
		<div class="widget-block"><?php echo apply_filters('the_content', $widget['sidebar_widget_content']); ?></div>
	<?php
	endforeach;
}