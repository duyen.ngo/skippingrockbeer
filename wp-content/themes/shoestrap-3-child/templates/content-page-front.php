<?php

// Check if the sections field has rows of data
if( have_rows('sections') ):
	// Loop through the sections
	while ( have_rows('sections') ) : the_row();
		
		// Get section template base on section type
		$type = get_row_layout();
		ss_get_template_part( "templates/sections/$type" );

	endwhile;

endif;

?>