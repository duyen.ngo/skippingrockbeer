<div class="tsticky_bottom"></div>
<?php global $ss_framework; ?>
<footer id="page-footer" class="content-info" role="contentinfo">
	<div class="footer-inner">
		<?php echo $ss_framework->open_container( 'div' ); ?>
			<?php shoestrap_footer_content(); ?>
		<?php echo $ss_framework->close_container( 'div' ); ?>
		<div class="footer-space"></div>
	</div>
	
</footer>