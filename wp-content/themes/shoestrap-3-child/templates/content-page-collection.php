<?php

// Check if the sections field has rows of data
if( have_rows('sections') ):
	// Loop through the sections
	while ( have_rows('sections') ) : the_row();

		// Get section template base on section type
		$type = get_row_layout();
		ss_get_template_part( "templates/sections/$type" );

	endwhile;

endif;
?>

<!-- <div class="section-content">
	<div class="default-page-body">
		<div class="page-body">
			<div class="button-group filters-button-group ourbeerBtn">
				<button onclick="filter()"  class="button is-checked" data-filte class="button is-checked" data-filter="*">What's on tap</button>
				<button onclick="filter()" class="button" data-filter=".transition">Core</button>
				<button onclick="filter()" class="button" data-filter=".alkali, .alkaline-earth">Seasonal</button>
			</div>
			<div class="beers-listing">
				<div class="element-item our-beer col-xs-12 col-sm-6 col-lg-4 transition metal" data-category="transition">
					<div class="beer-image">
						<img src="https://d2on2jh17533da.cloudfront.net/uploads/2018/08/beer-bottleglass.jpg" alt="">
					</div>
					<div s="beer-info">

					</div>
				</div>

				<div class="element-item our-beer col-xs-12 col-sm-6 col-lg-4 alkali metal" data-category="alkali">
					<div class="beer-image">
						<img src="https://d2on2jh17533da.cloudfront.net/uploads/2018/08/Zeplin_2018-09-25_14-18-081.jpg" alt="">
					</div>
					<div s="beer-info">

					</div>
				</div>

				<div class="element-item our-beer col-xs-12 col-sm-6 col-lg-4 alkaline-earth metal" data-category="alkaline-earth">
					<div class="beer-image">
						<img src="https://d2on2jh17533da.cloudfront.net/uploads/2018/08/Zeplin_2018-09-25_14-18-081.jpg" alt="">
					</div>
					<div s="beer-info">

					</div>
				</div>
			</div>
		</div>
	</div>
</div> -->