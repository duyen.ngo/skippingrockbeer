<?php

while ( have_posts() ) : the_post();
	global $post;

	$image = get_the_post_thumbnail_url( $post->ID, 'full' );
	$has_sidebar = get_field('has_sidebar');
	$sidebar_position = get_field('sidebar_position');
?>
	
	<div class="page-content default-page-content">

	<?php	
		$has_sidebar = get_field('has_sidebar');
		$sidebar_position = get_field('sidebar_position');

		if ($has_sidebar):
		?>
			<div class="page-wrapper row sidebar-<?php echo $sidebar_position ?>">

				<div class="content-wrapper col-md-9">
					<?php
						// Default page sections 
						ss_get_template_part( 'templates/content', 'page' );
					?>
				</div>

				<div class="sidebar-wrapper col-md-3">
					<?php
						// Page sidebar
						ss_get_template_part( 'templates/content', 'sidebar' );
					?>
				</div>
			</div>
		<?php else:
			// Default page Content
			ss_get_template_part( 'templates/content', 'page' );

		endif;
	?>
	</div>

<?php
	endwhile;
?>

