<<<<<<< HEAD
<div class="page-404-content">
	<h1>404</h1>
	<h2>Oops. The page you were looking for doesn't exist</h2>
	<p>You may have mistyped the address or the page may have moved.</p>
	<a href="/">Take me back to the homepage</a>
</div>
=======
<div class="page-404">
	<div class="center">
		<h1>404</h1>
		<h2>Oops. The page you were looking for doesn't exist</h2>
		<p>You may have mistyped the address or the page may have moved.</p>
		<a href="/">Take me back to the homepage.</a>
	</div>
</div>

<style type="text/css">
	.error404 #contentWrap {
		overflow-y: auto;
		min-height: 500px;
	}

	.page-404 {
		position: relative;
		height: calc(100vh - 80px);
		min-height: calc(100vh - 65px);
		background: #fd9f1e;
		text-align: center;
	}

	.page-404 .center {
		position: absolute;
		width: 100%;
		padding: 0 15px;
		top: 50%;
		left: 50%;
		-moz-transform: translate(-50%, -50%);
		-webkit-transform: translate(-50%, -50%);
		-ms-transform: translate(-50%, -50%);
		-o-transform: translate(-50%, -50%);
		transform: translate(-50%, -50%);
	}

	.page-404 p, .page-404 h1, .page-404 h2, .page-404 a {
		color: #000;
		margin: 0 0 20px;
		line-height: 1.4;
	}

	.page-404 h1 {
		font-size: 130px;
	}

	.page-404 h2, .page-404 p, .page-404 a {
		font-size: 24px;
	}

	.page-404 a {
		border-bottom: 1px solid;
		text-decoration: none;
	}

	.page-404 a:hover {
		color: #333;
	}

	@media only screen and (max-width: 1280px) {
		.page-404 {
			height: calc(100vh - 175px);
		}
	}

	@media only screen and (max-width: 650px) {
		.page-404 h1 {
			font-size: 60px;
		}

		.page-404 h2, .page-404 p, .page-404 a {
			font-size: 16px;
		}
	}
</style>
>>>>>>> d79f9e8b65862a927cb001ae03bc47aafcbd8305
