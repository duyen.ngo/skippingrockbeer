<?php
/*
Template Name: Collection page
*/

while ( have_posts() ) : the_post();
	global $post;

	$image = get_the_post_thumbnail_url( $post->ID, 'full' );
	$has_sidebar = get_field('has_sidebar');
	$sidebar_position = get_field('sidebar_position');
?>

	<div class="page-content collection-page-content">

		<!-- <div class="page-header" style="background-image:url(<?php echo $image; ?>);">
			<div class="page-header-inner">
				<h1 class="header-title"><?php echo get_the_title(); ?></h1>
				<div class="header-content"><?php the_content(); ?></div>
			</div>
		</div> -->

		

		<?php	if ($has_sidebar): ?>
			<div class="page-wrapper row sidebar-<?php echo $sidebar_position ?>">

				<div class="section-wrapper col-md-9">
					<?php
						// Collection page sections 
						include( 'templates/content-page-collection.php' );
					?>
				</div>

				<div class="sidebar-wrapper col-md-3">
					<?php
						// Page sidebar
						include( 'templates/content-sidebar.php' );
					?>
				</div>
			</div>
		<?php else:

			// Collection page sections
			include( 'templates/content-page-collection.php' );

		endif; ?>

	</div>

<?php
	endwhile;
?>