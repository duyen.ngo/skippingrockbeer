jQuery(document).ready(function ($) {
    'use strict'; 
});

window.onscroll = function() {
    growShrinkLogo();
};



function growShrinkLogo() {
    var logo = document.getElementById("site-logo");
    var text = document.getElementById("header-title");
    var media = document.getElementById("social-media");
    var bannerHeader = document.getElementById("banner-header");
    

    if (document.body.scrollTop > 3 || document.documentElement.scrollTop > 3) {
    logo.style.width = '70px';
    logo.style.height = '70px';
    text.style.fontSize= '16px';
    media.style.display= 'none';
    bannerHeader.style.background= 'rgba(0,0,0,.65)';
    

    } else {
        logo.style.width = '80px';
        logo.style.height = '80px';
        text.style.fontSize= '20px';
        media.style.display= 'block';
        bannerHeader.style.background= 'transparent';
        
    }   
}



function toggleBtnOnclick() {
    var btn = document.getElementById("myBtn");
    var banner = document.getElementById("banner-header");

    if (btn.onclick) {
        banner.style.background= 'rgba(0,0,0,.65)';
    } else {
        banner.style.background= 'transparent';
    }
}

// demo
function filter() {

var $grid = $(".beers-listing").isotope({
  itemSelector: ".element-item",
  layoutMode: "fitRows"
});
// filter functions
var filterFns = {
  // show if number is greater than 50
  numberGreaterThan50: function() {
    var number = $(this).find('.number').text();
    return parseInt( number, 10 ) > 50;
  },
  // show if name ends with -ium
  ium: function() {
    var name = $(this).find('.name').text();
    return name.match( /ium$/ );
  }
};
// bind filter button click
$('.filters-button-group').on( 'click', 'button', function() {
  var filterValue = $( this ).attr('data-filter');
  // use filterFn if matches value
  filterValue = filterFns[ filterValue ] || filterValue;
  $grid.isotope({ filter: filterValue });
});
// change is-checked class on buttons
$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});
}