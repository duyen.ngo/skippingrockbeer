<?php
/*
Template Name: Front page
*/
?>

<div class="page-content front-page-content">

	<?php
		// Front page sections
		ss_get_template_part( 'templates/content-page', 'front' );
	?>

</div>